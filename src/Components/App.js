import React from 'react';

import Navigation from './Navigation';
import AddItem from './AddItem';
import Home from './Home';
import Help from './Help';

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';


// This class sets up the routes for the navigation. 
// The route path determines the component displayed.
class App extends React.Component {

    render () {
        return (
            <Router>
                <div>
                    <Navigation />
                    <Switch>
                        <Route path="/" exact component={Home} />
                        <Route path="/add-item" component={AddItem} />
                        <Route path="/help" component={Help} />
                    </Switch>
                </div>
            </Router>
        )
    }
}

export default App;