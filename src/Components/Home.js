import React from 'react';
import Banner from './Banner';
import SearchBar from './SearchBar';
import CellarList from './CellarList';

import axios from 'axios'

import { Row, Col } from 'react-bootstrap';


class Home extends React.Component {

    // Establishes a global state for the app. searchTerm will be 
    // set by SearchBar.js, beerList and originalBeerList are assigned the cellared beer data.
    // beerList will be modified based on text entered in the searchBar, and if the search filter
    // is reset, then the the originalBeerList state will be set on beerList.

    state = {searchTerm: "", beerList: "", originalBeerList: ""}      

    // Get data from the database on page load.
    componentDidMount() {

        axios.get(process.env.REACT_APP_BACKEND_URL + '/beer-cellar')
        .then((res) => {
            this.setState({beerList: res.data, originalBeerList: res.data})
        })
        .catch((err) => {
            console.log(err)
        })
    }

    // Gets called when SearchBar.js detects a change.
    // Check if the text entered in the search bar matches either a brewery or beerName in the data.
    // If there is a match then filter the results to match the text input.
    searchBeerList = (term) => {
        this.setState({searchTerm: term})   // Receives search term from SearchBar.js

        let searchFilter = this.state.beerList.filter((filtered) => {

            return filtered.brewery.includes(this.state.searchTerm) || filtered.beerName.includes(this.state.searchTerm)
            
        })
        
        this.setState({beerList: searchFilter})
    }

    // When the 'X' on SearchBar.js is hit, we reset the list.
    resetFilter = () => {
        this.setState({beerList: this.state.originalBeerList, searchTerm: ""})
    }

    render () {
        return (
                <div>
                    <Row>
                        <Col>
                            <Banner />
                        </Col>
                    </Row>
                    <Row className="justify-content-center">
                        <Col xs={11} sm={10} md={10} lg={8}>
                            <SearchBar onChange={this.searchBeerList} searchTerm={this.state.searchTerm} cancelSearch={this.resetFilter} />
                            <CellarList beerList={this.state.beerList} />
                        </Col>
                    </Row>
                </div>
        )
    }
}

export default Home;