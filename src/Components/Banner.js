import React from 'react';

import { Jumbotron, Container } from 'react-bootstrap';

import '../CSS/Banner.css';


// Basic component for the banner at the top of the page.

const Banner = () => {
    return (
        <div>
            <Jumbotron fluid>
                <Container>
                    <h1>Beer Cellar</h1>
                        <p>
                        View your beer cellar stock here.
                        </p>
                </Container>
            </Jumbotron>
        </div>
    )
}

export default Banner;