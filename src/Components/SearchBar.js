import React from 'react';

import { InputGroup, FormControl, Button } from 'react-bootstrap';
import '../CSS/SearchBar.css';


class SearchBar extends React.Component {

    // Called when the search bar below detects a change. 
    // Takes the text input and passes it back to Home.js through props.
    searchList = (e) => {
        

        this.props.onChange(e.target.value)
    }
    

    render() {
        return (
            <div>
                <InputGroup className="mb-3">
                    <FormControl
                    placeholder="Search your beer cellar"
                    value={this.props.searchTerm}
                    aria-label="Search your beer cellar"
                    aria-describedby="basic-addon2"
                    onChange={this.searchList}
                    />
                    <InputGroup.Append>
                        <Button onClick={this.props.cancelSearch} variant="warning"><strong>Reset Filter</strong></Button>
                    </InputGroup.Append>
                </InputGroup>
            </div>
        )
    }
}

export default SearchBar;