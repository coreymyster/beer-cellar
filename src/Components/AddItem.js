import React from 'react';
import { Row, Col, Form, Button, Pagination } from 'react-bootstrap';
import axios from 'axios';

import Notification from './Notification'

// Nothing happening here yet - just a placeholder for when 
// functionality is built to add a new item to the cellar.

class AddItem extends React.Component {

    state = {
        brewery: "",
        beerName: "",
        abv: "",
        quantity: 1,
        yearBottled: "",
        cellarDate: "",
        style: "",
        size: "",
        showAlert: false,
        alertType: "",
        alertHeading: "",
        alertBody: ""
    }

    componentDidMount() {
        this.getCurrentDate()
    }

    getCurrentDate() {
        let d = new Date();
        this.setState({cellarDate: `${d.getFullYear()}`}) 
    }

// Adds data the user entered into the MongoDB database
    addBeer = (e) => {

        e.preventDefault();

        // The brewery and beer name are at least required for the app to be useful.
        // Here we check to make sure those fields are filled in. If not an error message is thrown
        // and data is not sent to the DB. If successful then the item gets added to the DB.
        if (this.state.brewery === "" || this.state.beerName === "") {
            this.setState({
                showAlert: true,
                alertType: "danger",
                alertHeading: "Error",
                alertBody: "A brewery and/or beer name is required."
            })
        } else {
            axios.post(process.env.REACT_APP_BACKEND_URL + "/add-item", {
                brewery: this.state.brewery,
                beerName: this.state.beerName,
                abv: this.state.abv,
                quantity: this.state.quantity,
                yearBottled: this.state.yearBottled,
                cellarDate: this.state.cellarDate,
                style: this.state.style,
                size: this.state.size
            })
            .then((res) => {
                console.log(res)
            })
            // If there is an errorm, error data will be passed to the notification component
            .catch(err => {
                console.log(err)
                this.setState({
                    showAlert: true,
                    alertType: "danger",
                    alertHeading: "Error",
                    alertBody: "An error occured while trying to add the item to the cellar. Please try again."
                })
            })
            // Once the form is submitted, clear all values from text inputs and 
            // send the user a notification of a successful submission.
            this.setState({
                brewery: "",
                beerName: "",
                abv: "",
                quantity: 1,
                yearBottled: "",
                cellarDate: "",
                style: "",
                size: "",
                showAlert: true,
                alertType: "success",
                alertHeading: "Item Added",
                alertBody: "You successfully added a beer to your cellar!"
            }) 
        }        
    }

    // Allows the user to close the notification banner once it appears
    closeAlert = () => {
        this.setState({showAlert: false})
    }

    render() {
        return (
            <Row className="justify-content-center">
                <Col xs={11} sm={10} md={10} lg={8}>
                <Notification alertType={this.state.alertType} showAlert={this.state.showAlert} alertHeading={this.state.alertHeading} alertBody={this.state.alertBody} closeNotification={this.closeAlert}/>
                    <Form method="post" onSubmit={this.addBeer}>
                        <Row>
                            <Col>
                                <Form.Group controlId="exampleForm.ControlInput1">
                                    <Form.Label>Brewery Name</Form.Label>
                                    <Form.Control type="text" value={this.state.brewery} onChange={(e) => {this.setState({brewery: e.target.value})}} />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group controlId="exampleForm.ControlInput1">
                                    <Form.Label>Beer Name</Form.Label>
                                    <Form.Control type="text" value={this.state.beerName} onChange={(e) => {this.setState({beerName: e.target.value})}} />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group controlId="exampleForm.ControlInput1">
                                    <Form.Label>ABV</Form.Label>
                                    <Form.Control type="text" value={this.state.abv} onChange={(e) => {this.setState({abv: e.target.value})}} />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group controlId="exampleForm.ControlInput1">
                                    <Form.Label>Quantity</Form.Label>
                                    <Pagination>
                                        <Pagination.Item onClick={() => {
                                            if(this.state.quantity <= 1) {
                                                this.setState({quantity: 1})
                                            } else {
                                                this.setState({quantity: this.state.quantity - 1})
                                            }
                                        }}>-</Pagination.Item>
                                        <Pagination.Item>{this.state.quantity}</Pagination.Item>
                                        <Pagination.Item onClick={() => this.setState({quantity: this.state.quantity + 1})}>+</Pagination.Item>
                                    </Pagination>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group controlId="exampleForm.ControlInput1">
                                    <Form.Label>Year Bottled</Form.Label>
                                    <Form.Control type="number" value={this.state.yearBottled} onChange={(e) => {this.setState({yearBottled: e.target.value})}} />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group controlId="exampleForm.ControlInput1">
                                    <Form.Label>Year Cellared</Form.Label>
                                    <Form.Control type="number" value={this.state.cellarDate} onChange={(e) => {this.setState({cellarDate: e.target.value})}} />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form.Group controlId="exampleForm.ControlInput1">
                                    <Form.Label>Style</Form.Label>
                                    <Form.Control type="text" value={this.state.style} onChange={(e) => {this.setState({style: e.target.value})}} />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group controlId="exampleForm.ControlInput1">
                                    <Form.Label>Size</Form.Label>
                                    <Form.Control type="text" value={this.state.size} onChange={(e) => {this.setState({size: e.target.value})}} />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Button variant="primary" type="submit">Submit</Button>
                        </Row>
                    </Form>
                </Col>
            </Row>
        )

    }
}

export default AddItem;