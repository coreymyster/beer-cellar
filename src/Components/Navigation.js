import React from 'react';
import {Link} from 'react-router-dom';

import { Navbar, Nav } from 'react-bootstrap';

import '../CSS/Navigation.css';

// Component for the application navigation. Sets the link location/route for each item.
const Navigation = () => {
    return (
        <Navbar>
            <Navbar.Brand href="#home">Beer Cellar</Navbar.Brand>
            <Nav className="mr-auto topNav" as="ul">
                <Link to="/">
                    <Nav.Item as="li">Home</Nav.Item>
                </Link>
                <Link to="add-item">
                    <Nav.Item as="li">Add Item</Nav.Item>
                </Link>
                <Link to="help">
                    <Nav.Item as="li">Help</Nav.Item>
                </Link>
                
            </Nav>
        </Navbar>
    )
}

export default Navigation;