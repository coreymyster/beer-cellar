import React from 'react';
import axios from 'axios';

import { Table, Spinner } from 'react-bootstrap';
import '../CSS/CellarList.css'
import { FaWindowClose } from 'react-icons/fa';



const CellarList = (props) => {
        
        // Enables the user to delte an item from the list by clicking the 'X' icon.
        const deleteItem = (beerID) => {
            console.log(beerID)
            axios.delete(process.env.REACT_APP_BACKEND_URL + "/beer-cellar", {data: {beerID: beerID} })
            .then((res) => {
                console.log(res)
            })
            .catch(err => console.log(err)) 
        }

        // Get the current year to calculate how long a beer has been in the cellar.
        let d = new Date();
        let currentYear = d.getFullYear()

        let beers = ""

        // Recieves state as props from Home.js and maps through the array.
        // Data from the new "beers" array is disected and rendered to the screen.
        if (props.beerList !== "") {
             beers = props.beerList.map((beer) => 
                <tr key={beer._id}>  
                    <td>{beer.brewery}</td>
                    <td>{beer.beerName}</td>
                    <td>{beer.abv}%</td>
                    <td>{beer.cellarDate}</td>
                    <td>{currentYear - beer.cellarDate} yrs</td>
                    <td onClick={() => {deleteItem(beer._id)}} className="deleteColumn"><FaWindowClose /></td>
                </tr>
        )
        }

        // While waiting on data to be retrieved from the database, a loading indicator will be shown.
        // Once data is retrieved then the list will be displayed.
        const fetchBeerList = () => {
            if(props.beerList === "") {
                return  (
                <div className="loading">
                    <h3>Fetching Cellar List</h3>
                    <Spinner className="spinner" animation="grow" variant="warning" role="Loading" />
                    <Spinner className="spinner" animation="grow" variant="warning" role="Loading" />
                    <Spinner className="spinner" animation="grow" variant="warning" role="Loading" />
                </div>
                )
            } else {
                return (
                    <Table striped bordered hover size="sm">
                        <thead>
                            <tr>
                                <th>Brewery</th>
                                <th>Beer Name</th>
                                <th>ABV</th>
                                <th>Year Cellared</th>
                                <th>Cellar Age</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            {beers}
                        </tbody>
                    </Table>
                )
            }
        }
            
        return (
            fetchBeerList()
        )
    }


export default CellarList;