import React from "react";

import "../CSS/Help.css";

import { Row, Col } from "react-bootstrap";

// Simple component to render JSX content to the Help page.

const Help = () => {
  return (
    <Row className="justify-content-center">
      <Col xs={10} sm={10} md={10} lg={10}>
        <div className="helpBody">
          <h2>Welcome to the Beer Cellar</h2>
          <p>
            The main purpose of this app is to keep track of craft beers that a
            craft beer "snob" may have in their cellar. I am a big craft beer
            lover myself, and sometimes there are those rare beers that I just
            want to store away for a few years to age. I realized that I need a
            way to start keeping track of the beers that I store away, so that's
            where my idea for this application came in.
          </p>
          <p>
            I started building this application in January of 2020, so it's under 
            continuous development.The app is connected to a MongoDB database which 
            not only houses the app data, but allows for the ability to perform CRUD 
            operations.
          </p>
          <h3>What's the app built in?</h3>
          <p>
            This app is a single page application utilizing the following
            technologies:
          </p>
          <ul>
            <li>React JS</li>
            <li>React Router</li>
            <li>React Bootstrap</li>
            <li>Express</li>
            <li>Node JS</li>
            <li>MongoDB</li>
            <li>
              AWS - Specifically Amplify in AWS. Amplify allows me to host my web 
              application publicly and connect it to my code repo to establish a 
              CI/CD workflow. Additionally, my backend is set up through an EC2 instance.
            </li>
          </ul>
          <h3>Using the app</h3>
          <p>
            The app is pretty basic right now, so right now it's pretty simple. The home page of the app
            displays the beers in the user's cellar. If you want to search for a beer or filter down by
            brewery or beer name, then you can do so by utilizing the search bar. Beers can be added and removed from the cellar. Down the road I plan to create a "more information" page for each
            individual beer when it is clicked on in the list. And of course, there are areas that are going to
            need a more in depth look in terms of styling.
          </p>
        </div>
      </Col>
    </Row>
  );
};

export default Help;
