import React from 'react';

import { Alert } from 'react-bootstrap';

class Notification extends React.Component {

    // Modifies state of the notification banner to false 
    // so that it can be closed by the user.
    handleNotification = () => {
        this.props.closeNotification()
    }

    render() {
        // Notification receives success or error messages from the AddItem component. 
        // Notification message is dependent on whether or not an item was successfully added or not.
        return (
            <Alert variant={this.props.alertType} show={this.props.showAlert} onClick={this.handleNotification} dismissible>
                <Alert.Heading>{this.props.alertHeading}</Alert.Heading>
                <p>
                {this.props.alertBody}
                </p>
          </Alert>
        )
    }

}
export default Notification;